import Link from "next/link";
import Image from "next/image";
import { PrismaClient } from "@prisma/client";

async function getData() {
  const prisma = new PrismaClient();

  const cars = await prisma.car.findMany();

  prisma.$disconnect();  

  return { data: cars }
}

export default async function Home() {
  const data = await getData();
  return (
    <main>
      <div className="flex justify-center md:justify-end gap-2 mr-3">
        <div>
          <Link
            href="/formulario"
            id="Agregar vehiculo"
            aria-label="Create folder"
            className="select-none text-white hover:shadow-md bg-[#3B3170] hover:bg-black focus:outline-none font-medium rounded-lg inline-flex items-center px-5 py-2.5 mb-2 mt-2 border border-transparent"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="mr-2 -ml-1 w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M2.25 12.75V12A2.25 2.25 0 014.5 9.75h15A2.25 2.25 0 0121.75 12v.75m-8.69-6.44l-2.12-2.12a1.5 1.5 0 00-1.061-.44H4.5A2.25 2.25 0 002.25 6v12a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18V9a2.25 2.25 0 00-2.25-2.25h-5.379a1.5 1.5 0 01-1.06-.44z"
              />
            </svg>
            Agregar vehiculo
          </Link>
        </div>
      </div>

      <div className="ml-4 m-3">
        <div className="flex justify-center items-center">
          <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-3">
            {data?.data.map((car) => {
              return (
                <div key={car.id} className="mt-4">
                  <div className="max-w-sm bg-elements rounded-lg">
                    <Image
                      className="rounded-t-lg"
                      src={`/api/image/${JSON.parse(car.images)[0].name}`}
                      alt=""
                      width={382}
                      height={236}
                    />
                    <div className="p-3">
                      <h5 className="mb-2 text-2xl font-bold tracking-tight">
                        {car.year} {car.brand} {car.model}{" "}
                        {car.color.toLowerCase()}{" "}
                        {car.transmission.toLocaleLowerCase()}
                      </h5>
                      <p className="mb-3 font-normal">
                        {car.fuelType} - {car.traction} - {car.numberPassengers}{" "}
                        pasajeros -{" "}
                        {car.polarized === "SI"
                          ? "polarizado"
                          : "no polarizado"}{" "}
                        -{" "}
                        {car.airConditioning === "SI"
                          ? "con aire acondicionado"
                          : "sin aire acondicionado"}
                      </p>
                      <div className="flex justify-between">
                        <div className="flex justify-center items-center">
                          <span className="text-xl font-bold">
                            ${car.price} USD
                          </span>
                        </div>

                        <button className="text-white bg-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="mr-2 -ml-1 w-5 h-5"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                            />
                          </svg>
                          Eliminar
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </main>
  );
}
