"use client";
import Image from "next/image";
import { ReactSortable } from "react-sortablejs";
import { useState, ChangeEvent, useRef, useContext } from "react";
import Link from "next/link";
import { useRouter } from 'next/navigation';

// CONTEXT
import CarContext from "../../../context/CarContext";
import { GeneralDataContextType } from "../../../context/CarContext/types";

function Images() {
  const { GeneralData } = useContext(CarContext) as GeneralDataContextType;
  const {push} = useRouter();

  const inputRef = useRef<HTMLInputElement>(null);

  const [ShowInput, setShowInput] = useState(false);

  const [Files, setFiles] = useState<Array<File> | null>(null);
  const [FilesName, setFilesName] = useState<
    Array<{ id: number; name: string }>
  >([]);

  const handleFiles = (e: ChangeEvent<HTMLInputElement>) => {
    if (Files === null) {
      // @ts-ignore
      setFiles([...e.target.files]);
    } else {
      // @ts-ignore
      setFiles([...Files, ...e.target.files]);
    }
    let filenames: Array<{ id: number; name: string }> = FilesName;
    //@ts-ignore
    for (const iterator of e.target.files) {
      filenames.push({
        id: filenames.length + 1,
        name: iterator?.name,
      });
    }
    setFilesName(filenames);
    setShowInput(false);
  };

  const [FileSelected, setFileSelected] = useState<string | ArrayBuffer>("");

  const changeImagenUpdate = (file: File) => {
    const imagen = file;

    //convierto la imagen en url para poder mostrarla en la interfaz
    const reader = new FileReader();
    reader.readAsDataURL(imagen);
    reader.onload = (e) => {
      e.preventDefault();
      // @ts-ignore
      setFileSelected(e.target?.result); // le damos el binario de la imagen para mostrarla en pantalla
    };
  };

  const removeFileList = (filenameSelected: string) => {
    const unSelectedFiles = FilesName.filter(
      (filename) => filename.name !== filenameSelected
    );

    const unSelectedFilesData = Files?.filter(
      (filename) => filename.name !== filenameSelected
    );

    setFiles(unSelectedFilesData as Array<File>);
    setFilesName(unSelectedFiles);
  };

  const [Loading, setLoading] = useState(false);

  const uploadFiles = async () => {
    setLoading(true);
    const formdata = new FormData();
    FilesName.forEach((filename, index) => {
      // @ts-ignore
      formdata.append("files", Files[index], filename.name);
    });
    const response = await (
      await fetch("/api/upload/file", {
        method: "POST",
        body: formdata,
      })
    ).json();

    await (
      await fetch("/api/post/car", {
        method: "POST",
        body: JSON.stringify({
          carType: GeneralData["Tipo de vehiculo"],
          brand: GeneralData.Marca,
          model: GeneralData.Modelo,
          color: GeneralData.Color,
          fuelType: GeneralData["Tipo de combustible"],
          polarized: GeneralData.Polarizado,
          traction: GeneralData.Tracción,
          NumberPassengers: GeneralData["Número de pasajeros"],
          airConditioning: GeneralData["Aire acondicionado"],
          electricWindows: GeneralData["Ventanas eléctricas"],
          playerScreen: GeneralData["Reproductor con pantalla"],
          smartKeys: GeneralData["llave inteligente"],
          year: GeneralData["Año"],
          images: JSON.stringify(response),
          price: GeneralData.Precio,
          transmission: GeneralData["Transmisión"],
        }),
      })
    ).json();

    setLoading(false);

    push("/");
  };

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 h-[95vh]">
      <div>
        <div className="flex justify-center items-center">
          <div
            className={`w-full m-3 md:m-5 h-80 ${
              ShowInput ? "bg-black" : "bg-menu"
            } rounded-lg border`}
            onDragLeave={(e) => {
              e.preventDefault();
              setShowInput(true);
            }}
            onDragExit={(e) => {
              e.preventDefault();
              console.log("termino");

              setShowInput(false);
            }}
          >
            <input
              className={`w-full h-80 cursor-pointer text-center ${
                ShowInput ? "block opacity-0" : "hidden"
              }`}
              type="file"
              accept="image/*"
              multiple={true}
              onChange={handleFiles}
              ref={inputRef}
            />
            <div
              className={`${
                ShowInput ? "hidden" : ""
              } grid h-80 place-content-center p-10`}
            >
              <div className="flex justify-center items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w- h-16"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 14.25v-2.625a3.375 3.375 0 00-3.375-3.375h-1.5A1.125 1.125 0 0113.5 7.125v-1.5a3.375 3.375 0 00-3.375-3.375H8.25m2.25 0H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 00-9-9z"
                  />
                </svg>
              </div>
              <h3 className="mt-7 text-xl">
                Drag files here to add them to your folder or{" "}
                <span
                  className="text-indigo-300 hover:underline cursor-pointer"
                  onClick={() => inputRef.current?.click()}
                >
                  choose your files
                </span>
              </h3>
            </div>
          </div>
        </div>

        <div className="scroll-files h-60 mt-8 m-3">
          <ReactSortable list={FilesName} setList={setFilesName}>
            {FilesName.map((filename) => {
              return (
                <div
                  key={filename.id}
                  className="w-90 h-14 p-4 bg-menu rounded-lg mb-5 mr-2 cursor-move"
                  onClick={() => {
                    // @ts-ignore
                    for (const file of Files) {
                      if (file.name === filename.name) {
                        changeImagenUpdate(file);
                      }
                    }
                  }}
                  onDragStart={() => {
                    // @ts-ignore
                    for (const file of Files) {
                      if (file.name === filename.name) {
                        changeImagenUpdate(file);
                      }
                    }
                  }}
                >
                  <div className="flex justify-between">
                    <div className="inline-flex gap-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M19.5 14.25v-2.625a3.375 3.375 0 00-3.375-3.375h-1.5A1.125 1.125 0 0113.5 7.125v-1.5a3.375 3.375 0 00-3.375-3.375H8.25m2.25 0H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 00-9-9z"
                        />
                      </svg>
                      {filename.name}
                    </div>
                    <div>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 hover:cursor-pointer"
                        onClick={() => removeFileList(filename.name)}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M6 18L18 6M6 6l12 12"
                        />
                      </svg>
                    </div>
                  </div>
                </div>
              );
            })}
          </ReactSortable>
        </div>

        <div className="hidden md:block">
          <div className=" flex justify-center items-center">
            <footer className="fixed bottom-0 w-96 bg-elements h-20 rounded-t-xl">
              <div className="flex justify-between m-3">
                <div>
                  <Link
                    href="/formulario"
                    id="atras"
                    aria-label="atras"
                    className="select-none text-white hover:shadow-md bg-[#3B3170] hover:bg-black focus:outline-none font-medium rounded-lg inline-flex items-center px-5 py-2.5 mb-2 mt-2 border border-transparent"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="mr-2 -ml-1 w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M9 15L3 9m0 0l6-6M3 9h12a6 6 0 010 12h-3"
                      />
                    </svg>
                    Atras
                  </Link>
                </div>
                <div>
                  <button
                    id="guardar"
                    aria-label="guardar"
                    onClick={uploadFiles}
                    className="select-none text-white hover:shadow-md bg-[#3B3170] hover:bg-black focus:outline-none font-medium rounded-lg inline-flex items-center px-5 py-2.5 mb-2 mt-2 border border-transparent"
                  >
                    {Loading ? (
                      <svg
                        aria-hidden="true"
                        role="status"
                        className="inline mr-2 -ml-1 w-6 h-6 animate-spin"
                        viewBox="0 0 100 101"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                          fill="#E5E7EB"
                        />
                        <path
                          d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                          fill="currentColor"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="mr-2 -ml-1 w-6 h-6"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M15 15l6-6m0 0l-6-6m6 6H9a6 6 0 000 12h3"
                        />
                      </svg>
                    )}
                    Guardar
                  </button>
                </div>
              </div>
            </footer>
          </div>
        </div>

        <div className="block md:hidden flex justify-between m-3">
          <div>
            <Link
              href="/formulario"
              id="atras"
              aria-label="atras"
              className="select-none text-white hover:shadow-md bg-[#3B3170] hover:bg-black focus:outline-none font-medium rounded-lg inline-flex items-center px-5 py-2.5 mb-2 mt-2 border border-transparent"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="mr-2 -ml-1 w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M9 15L3 9m0 0l6-6M3 9h12a6 6 0 010 12h-3"
                />
              </svg>
              Atras
            </Link>
          </div>
          <div>
            <button
              id="guardar"
              aria-label="guardar"
              onClick={uploadFiles}
              className="select-none text-white hover:shadow-md bg-[#3B3170] hover:bg-black focus:outline-none font-medium rounded-lg inline-flex items-center px-5 py-2.5 mb-2 mt-2 border border-transparent"
            >
              {Loading ? (
                <svg
                  aria-hidden="true"
                  role="status"
                  className="inline mr-2 -ml-1 w-6 h-6 animate-spin"
                  viewBox="0 0 100 101"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                    fill="#E5E7EB"
                  />
                  <path
                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                    fill="currentColor"
                  />
                </svg>
              ) : (
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="mr-2 -ml-1 w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 15l6-6m0 0l-6-6m6 6H9a6 6 0 000 12h3"
                  />
                </svg>
              )}
              Guardar
            </button>
          </div>
        </div>
      </div>
      <div className="hidden md:block bg-white">
        <div className="grid h-[95vh] place-content-center m-3">
          {FileSelected === "" ? (
            <div>
              <div className="flex justify-center items-center">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="#141627"
                  className="w-16 h-16"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M12 18v-5.25m0 0a6.01 6.01 0 001.5-.189m-1.5.189a6.01 6.01 0 01-1.5-.189m3.75 7.478a12.06 12.06 0 01-4.5 0m3.75 2.383a14.406 14.406 0 01-3 0M14.25 18v-.192c0-.983.658-1.823 1.508-2.316a7.5 7.5 0 10-7.517 0c.85.493 1.509 1.333 1.509 2.316V18"
                  />
                </svg>
              </div>

              <p className="text-center mt-7 text-[#141627]">
                Selecciona las imágenes para subirlas al servidor.
              </p>
            </div>
          ) : (
            <>
              {/* @ts-ignore */}
              <Image src={FileSelected} width={900} height={900} alt="" />
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default Images;
