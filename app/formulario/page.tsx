"use client";
import { ChangeEvent, useContext } from "react";
import Link from "next/link";

// CONTEXT
import CarContext from "../../context/CarContext";
import { GeneralDataContextType } from "../../context/CarContext/types";

function getDates() {
  const today = new Date();
  const dates = [];
  for (let i = 0; i < 14; i++) {
    const date = today.getFullYear() - i;
    dates.push(date);
  }
  return dates;
}

function Form() {
  const { GeneralData, setGeneralData } = useContext(
    CarContext
  ) as GeneralDataContextType;

  const changeInput = (
    e: ChangeEvent<HTMLSelectElement | HTMLInputElement>
  ) => {
    setGeneralData((prevState) => {
      return {
        ...prevState,
        [e.target.name]:
          e.target.type === "number"
            ? parseInt(e.target.value)
            : e.target.value,
      };
    });
  };

  return (
    <>
      <div className="flex justify-center items-center">
        <div className="max-w-5xl m-3">
          <p className="font-bold text-xl text-center mt-3 mb-4">
            Detalles generales
          </p>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-3">
            {datosGenerales.map(({ id, name, typeInput, options }) => {
              return (
                <div key={id}>
                  <label htmlFor="name">
                    {name}
                    {typeInput === "listbox" && (
                      <select
                        name={name}
                        // @ts-ignore
                        value={GeneralData[name]}
                        onChange={(e) => changeInput(e)}
                        className="mt-2 appearance-none border border-gray-700 focus:border-white bg-menu rounded-lg w-full py-3 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
                      >
                        {options.map((option) => {
                          return (
                            <option key={option} value={option}>
                              {option}
                            </option>
                          );
                        })}
                      </select>
                    )}

                    {typeInput === "text" && (
                      <input
                        className="mt-2 appearance-none border border-gray-700 focus:border-white bg-menu rounded-lg w-full py-3 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
                        id={name}
                        placeholder=""
                        type="text"
                        name={name}
                        // @ts-ignore
                        value={GeneralData[name]}
                        onChange={changeInput}
                      />
                    )}
                    {typeInput === "number" && (
                      <input
                        className="mt-2 appearance-none border border-gray-700 focus:border-white bg-menu rounded-lg w-full py-3 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
                        id={name}
                        type="number"
                        name={name}
                        // @ts-ignore
                        value={GeneralData[name]}
                        min={1}
                        onChange={changeInput}
                      />
                    )}
                  </label>
                </div>
              );
            })}

            <div />
            <div />
          </div>

          <p className="font-bold text-xl text-center mt-3 mb-4">
            Detalles del interior
          </p>

          <div className="grid grid-cols-1 md:grid-cols-2 gap-3">
            {datosInterior.map(({ id, name, typeInput, options }) => {
              return (
                <div key={id}>
                  <label htmlFor="name">
                    {name}
                    {typeInput === "listbox" && (
                      <select
                        name={name}
                        // @ts-ignore
                        value={GeneralData[name]}
                        onChange={changeInput}
                        className="mt-2 appearance-none border border-gray-700 focus:border-white bg-menu rounded-lg w-full py-3 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
                      >
                        {options.map((option) => {
                          return (
                            <option key={option} value={option}>
                              {option}
                            </option>
                          );
                        })}
                      </select>
                    )}

                    {typeInput === "text" && (
                      <input
                        className="mt-2 appearance-none border border-gray-700 focus:border-white bg-menu rounded-lg w-full py-3 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
                        id={name}
                        placeholder=""
                        type="text"
                        name={name}
                        // @ts-ignore
                        value={GeneralData[name]}
                        onChange={changeInput}
                      />
                    )}
                    {typeInput === "number" && (
                      <input
                        className="mt-2 appearance-none border border-gray-700 focus:border-white bg-menu rounded-lg w-full py-3 px-3 text-white leading-tight focus:outline-none focus:shadow-outline"
                        id={name}
                        placeholder=""
                        type="number"
                        name={name}
                        // @ts-ignore
                        value={GeneralData[name]}
                        onChange={changeInput}
                      />
                    )}
                  </label>
                </div>
              );
            })}
          </div>

          <div className="flex justify-end items-end mt-3">
            <Link
              href="/formulario/imagenes"
              id="siguiente"
              aria-label="siguiente"
              className="select-none text-white hover:shadow-md bg-[#3B3170] hover:bg-black focus:outline-none font-medium rounded-lg inline-flex items-center px-5 py-2.5 mb-2 mt-2 border border-transparent"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="mr-2 -ml-1 w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15 15l6-6m0 0l-6-6m6 6H9a6 6 0 000 12h3"
                />
              </svg>
              Siguiente
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default Form;

const datosGenerales = [
  {
    id: 1,
    name: "Tipo de vehiculo",
    typeInput: "listbox",
    options: ["Sedan", "Camioneta", "Pickup"],
  },
  {
    id: 3,
    name: "Transmisión",
    typeInput: "listbox",
    options: ["Automático", "Standar"],
  },
  {
    id: 4,
    name: "Marca",
    typeInput: "listbox",
    options: [
      "Toyota",
      "Nissan",
      "Chevrolet",
      "Ford",
      "Jeep",
      "BMW",
      "Mitsubishi",
      "Kia",
      "Volkswagen",
      "Honda",
      "Mazda",
      "Hyundai",
      "Mercedes-Benz",
    ],
  },
  {
    id: 5,
    name: "Año",
    typeInput: "listbox",
    options: getDates(),
  },
  {
    id: 6,
    name: "Modelo",
    typeInput: "text",
    options: [],
  },
  {
    id: 7,
    name: "Color",
    typeInput: "listbox",
    options: [
      "Rojo",
      "Azul",
      "Verde",
      "Amarillo",
      "Negro",
      "Blanco",
      "Gris",
      "Naranja",
      "Rosado",
      "Celeste",
      "Morado",
    ],
  },
  {
    id: 8,
    name: "Tipo de combustible",
    typeInput: "listbox",
    options: ["Diesel", "Gasolina"],
  },
  {
    id: 9,
    name: "Polarizado",
    options: ["SI", "NO"],
    typeInput: "listbox",
  },
  {
    id: 10,
    name: "Tracción",
    typeInput: "listbox",
    options: ["FWD", "RWD", "AWD", "4WD", "4X4"],
  },
  {
    id: 11,
    name: "Número de pasajeros",
    typeInput: "number",
    options: [],
  },
  {
    id: 12,
    name: "Precio",
    typeInput: "number",
    options: [],
  },
];

const datosInterior = [
  {
    id: 1,
    name: "Aire acondicionado",
    typeInput: "listbox",
    options: ["SI", "NO"],
  },
  {
    id: 2,
    name: "Ventanas eléctricas",
    typeInput: "listbox",
    options: ["SI", "NO"],
  },
  {
    id: 3,
    name: "llave inteligente",
    typeInput: "listbox",
    options: ["SI", "NO"],
  },
  {
    id: 4,
    name: "Reproductor con pantalla",
    typeInput: "listbox",
    options: ["SI", "NO"],
  },
];
