"use client";
import { createContext, ReactNode, useState } from "react";

const Context = createContext({});

Context.displayName = "CarContext";

interface Props {
  children: ReactNode;
}

export function ContextCarProvider({ children }: Props) {
  const [GeneralData, setGeneralData] = useState({
    "Tipo de vehiculo": "Sedan",
    Transmisión: "Automático",
    Marca: "Toyota",
    Año: new Date().getFullYear(),
    Modelo: "",
    Color: "Rojo",
    "Tipo de combustible": "Diesel",
    Polarizado: "SI",
    Tracción: "FWD",
    "Número de pasajeros": 5,
    "Aire acondicionado": "SI",
    "Ventanas eléctricas": "SI",
    "llave inteligente": "SI",
    "Reproductor con pantalla": "SI",
    imagenes: "[]",
    Precio: 0,
  });
  return (
    <Context.Provider
      value={{
        GeneralData,
        setGeneralData,
      }}
    >
      {children}
    </Context.Provider>
  );
}

export default Context;
