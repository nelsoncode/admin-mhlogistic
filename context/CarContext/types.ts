import { Dispatch, SetStateAction } from "react";

export type GeneralDataType = {
  "Tipo de vehiculo": string;
  Transmisión: string;
  Marca: string;
  Año: string;
  Modelo: string;
  Color: string;
  "Tipo de combustible": string;
  Polarizado: string;
  Tracción: string;
  "Número de pasajeros": number;
  "Aire acondicionado": string;
  "Ventanas eléctricas": string;
  "llave inteligente": string;
  "Reproductor con pantalla": string;
  imagenes: string;
  Precio: 0;
};

export type GeneralDataContextType = {
  GeneralData: GeneralDataType;
  setGeneralData: Dispatch<SetStateAction<GeneralDataType>>;
};
