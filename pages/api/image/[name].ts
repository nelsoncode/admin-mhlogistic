import type { NextApiRequest, NextApiResponse } from "next";
import { S3 } from "@aws-sdk/client-s3";
import { GetObjectCommand } from "@aws-sdk/client-s3";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    try {
      const s3Client = new S3({
        forcePathStyle: false,
        endpoint: "https://sfo3.digitaloceanspaces.com",
        region: "sfo3",
        credentials: {
          accessKeyId: `${process.env.DO_SPACE_KEY}`,
          secretAccessKey: `${process.env.DO_SPACE_SECRET}`,
        },
      });

      const params = {
        Bucket: "carsmhlogistic",
        Key: `cars/${req.query.name}`,
      };

      const { Body, ContentType } = await s3Client.send(
        new GetObjectCommand(params)
      );

      res.setHeader("Content-Type", `${ContentType}`);
      res.setHeader(
        "Cache-Control",
        "public, s-maxage=10, stale-while-revalidate=60"
      );      

      res.status(200).send(Body);
    } catch (error: any) {
      res.status(200).json(error);
    }
  }
}
