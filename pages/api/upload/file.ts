import type { NextApiRequest, NextApiResponse } from "next";
import { S3 } from "@aws-sdk/client-s3";
import { PutObjectCommand } from "@aws-sdk/client-s3";
import formidable from "formidable";
import fs from "fs";
import { randomUUID } from "crypto";

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    try {
      const form = formidable({ multiples: true });
      const formData = new Promise((resolve, reject) => {
        form.parse(req, async (err, fields, files) => {
          if (err) {
            reject("error");
          }
          resolve({ fields, files });
        });
      });
      // @ts-ignore
      const { files } = await formData;

      const s3Client = new S3({
        forcePathStyle: false, // Configures to use subdomain/virtual calling format.
        endpoint: "https://sfo3.digitaloceanspaces.com",
        region: "sfo3",
        credentials: {
          accessKeyId: `${process.env.DO_SPACE_KEY}`,
          secretAccessKey: `${process.env.DO_SPACE_SECRET}`,
        },
      });

      const responseFile = [];

      // FOR MULTIPLES FILES
      if (Array.isArray(files.files)) {
        // @ts-ignore
        for (const file of files.files) {
          const nameUUID = `${randomUUID()}${file.originalFilename}`;
          const params = {
            Bucket: "carsmhlogistic",
            Key: `cars/${nameUUID}`,
            Body: fs.readFileSync(file.filepath),
            ACL: "private",
            ContentType: file.mimetype,
          };

          await s3Client.send(new PutObjectCommand(params));

          responseFile.push({
            originalName: file.originalFilename,
            name: nameUUID,
          });
        }
        res.status(200).json(responseFile);
      } else {
        const file = files.files;
        const nameUUID = `${randomUUID()}${file.originalFilename}`;
        const params = {
          Bucket: "carsmhlogistic",
          Key: `cars/${nameUUID}`,
          Body: fs.readFileSync(file.filepath),
          ACL: "private",
          ContentType: file.mimetype,
        };

        const response = await s3Client.send(new PutObjectCommand(params));

        responseFile.push({
          originalName: file.originalFilename,
          name: nameUUID,
        });
        const responseStatusCode = response["$metadata"][
          "httpStatusCode"
        ] as number;

        res.status(responseStatusCode).json(responseFile);
      }
    } catch (error: any) {
      res.status(500).json({ message: "error" });
    }
  }
}
