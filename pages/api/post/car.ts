// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    try {
      const body =
        typeof req.body === "string" ? JSON.parse(req.body) : req.body;

      await prisma.car.create({
        data: {
          carType: body.carType,
          airConditioning: body.airConditioning,
          brand: body.brand,
          color: body.color,
          fuelType: body.fuelType,
          electricWindows: body.electricWindows,
          model: body.model,
          images: body.images,
          polarized: body.polarized,
          smartKeys: body.smartKeys,
          traction: body.traction,
          playerScreen: body.playerScreen,
          numberPassengers: body.NumberPassengers,
          year: parseInt(body.year),
          price: body.price,
          transmission: body.transmission,
        },
      });
      res.status(200).json({ message: "success!" });
    } catch (error: any) {
      res.status(500).json(error);
    }
  }
}
