// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    try {
      const cars = await prisma.car.findMany();

      prisma.$disconnect();

      res.status(200).json({ data: cars });
    } catch (error: any) {
      res.status(500).json(error);
    }
  }
}
