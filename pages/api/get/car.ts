import type { NextApiRequest, NextApiResponse } from "next";
import { PrismaClient } from "@prisma/client";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    try {
      const prisma = new PrismaClient();

      const params = req.query;

      const car = await prisma.car.findUnique({
        where: {
          id: `${params?.id}`,
        },
      });

      prisma.$disconnect();

      res.status(200).json({ data: car });
    } catch (error: any) {
      res.status(404).json(error);
    }
  }
}
