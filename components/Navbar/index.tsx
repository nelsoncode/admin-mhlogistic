function Navbar() {
  return (
    <nav className="p-2 bg-menu  border border-l-transparent border-t-transparent border-r-transparent border-b-[#56545e]">
      <div className="flex justify-center items-center">
        <form className="flex items-center">
          <label htmlFor="voice-search" className="sr-only">
            Search
          </label>
          <div className="relative w-full">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="#94A3B8"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <input
              type="text"
              id="voice-search"
              className="bg-[#141627] text-sm rounded-lg block w-full pl-10 p-2.5"
              placeholder="Buscar vehiculos, pickups..."
              required
            />
          </div>
        </form>
      </div>
    </nav>
  );
}

export default Navbar;
